<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action='root';
if (!$zbp->CheckRights($action)) {$zbp->ShowError(6);die();}
if (!$zbp->CheckPlugin('isux')) {$zbp->ShowError(48);die();}
$blogtitle='主题配置';
$act = "";
if ($_GET['act']){
	$act = $_GET['act'] == "" ? 'config' : $_GET['act'];
}
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
if(isset($_POST['keywords'])){
	$zbp->Config('isux')->keywords = $_POST['keywords'];
	$zbp->Config('isux')->description = $_POST['description'];
	$zbp->Config('isux')->seo = $_POST['seo'];
	$zbp->SaveConfig('isux');
	$zbp->ShowHint('good');
}
?>
<div id="divMain">
	<div class="divHeader"><?php echo $blogtitle;?></div>
	<div class="SubMenu">
		<?php isux_SubMenu($act);?>
		<a href="http://www.songhaifeng.com/ZBlog/38.html" target="_blank"><span class="m-right">技术支持</span></a>
    </div>
	<div id="divMain2">
		<?php if ($act == 'config') { ?>
			<form enctype="multipart/form-data" method="post" action="save.php?type=logo">  
				<table width="100%" style='padding:0;margin:0;' cellspacing='0' cellpadding='0' class="tableBorder">
					<tr>
						<td width="15%"><label for="logo.png"><p align="center">logo（110*30）</p></label></td>
						<td width="50%"><p align="center"><input name="logo.png" type="file"/></p></td>
						<td width="25%"><p align="center"><input name="" type="Submit" class="button" value="保存"/></p></td>
					</tr>
				</table>
			</form>
			<form enctype="multipart/form-data" method="post" action="save.php?type=qrcode">  
				<table width="100%" style='padding:0;margin:0;' cellspacing='0' cellpadding='0' class="tableBorder">
					<tr>
						<td width="15%"><label for="qrcode.png"><p align="center">文章底部二维码（230*230）</p></label></td>
						<td width="50%"><p align="center"><input name="qrcode.png" type="file"/></p></td>
						<td width="25%"><p align="center"><input name="" type="Submit" class="button" value="保存"/></p></td>
					</tr>
				</table>
			</form>
			<form id="form1" name="form1" method="post">
				<table width="100%" style='padding:0;margin:0;' cellspacing='0' cellpadding='0' class="tableBorder">
					<tr>
						<th width="15%"><p align="center">配置名称</p></th>
						<th width="50%"><p align="center">配置内容</p></th>
						<th width="25%"><p align="center">配置说明</p></th>
					</tr>
					<tr>
						<td><label for="keywords"><p align="center">网站关键词</p></label></td>
						<td><p align="left"><input name="keywords" type="text" id="keywords" style="width:100%;" value="<?php echo $zbp->Config('isux')->keywords;?>" /></p></td>
						<td><p align="left">首页网站关键词</p></td>
					</tr>
					<tr>
						<td><label for="description"><p align="center">网站描述</p></label></td>
						<td><p align="left"><textarea name="description" type="text" id="description" style="height:50px;width:100%;"><?php echo $zbp->Config('isux')->description;?></textarea></p></td>
						<td><p align="left">首页网站描述</p></td>
					</tr>
					<tr>
						<td><label for="seo"><p align="center">SEO</p></label></td>
						<td>
							<p align="center">
								<select name="seo" id="seo">
									<option value="a" <?php if($zbp->Config('isux')->seo == 'a') echo 'selected'?>>打开</option>
									<option value="b" <?php if($zbp->Config('isux')->seo == 'b') echo 'selected'?>>关闭</option>
								</select>
							</p>
						</td>
						<td><p align="left">如果使用【SEO工具大全】这个插件，这里请打开。</p></td>
					</tr>
					<tr>
						<td><label for="post_category"><p align="center">文章是否显示分类名</p></label></td>
						<td>
							<p align="center">
								<select name="post_category" id="post_category">
									<option value="a" <?php if($zbp->Config('isux')->post_category == 'a') echo 'selected'?>>打开</option>
									<option value="b" <?php if($zbp->Config('isux')->post_category == 'b') echo 'selected'?>>关闭</option>
								</select>
							</p>
						</td>
						<td><p align="left">只显示当前分类，不显示父分类。<br/>SEO选项打开时此项设置无效。</p></td>
					</tr>
					<tr>
						<td><label for="page_subname"><p align="center">单页是否显示网站副标题</p></label></td>
						<td>
							<p align="center">
								<select name="page_subname" id="page_subname">
									<option value="a" <?php if($zbp->Config('isux')->page_subname == 'a') echo 'selected'?>>打开</option>
									<option value="b" <?php if($zbp->Config('isux')->page_subname == 'b') echo 'selected'?>>关闭</option>
								</select>
							</p>
						</td>
						<td><p align="left">单页面是否显示网站副标题。<br/>SEO选项打开时此项设置无效。</p></td>
					</tr>
				</table>
				<br/>
				<input name="" type="Submit" class="button" value="保存"/>
			</form>
		<?php } ?>
	</div>
</div>
<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>