<?php
require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'plugin/search_config.php'; // searchplus
RegisterPlugin("isux","ActivePlugin_isux");
function isux_SubMenu($id){
	$arySubMenu = array(
		0 => array('基本设置', 'config', 'left', false),
	);
	foreach($arySubMenu as $k => $v){
		echo '<a href="?act='.$v[1].'" '.($v[3]==true?'target="_blank"':'').'><span class="m-'.$v[2].' '.($id==$v[1]?'m-now':'').'">'.$v[0].'</span></a>';
	}
}
function ActivePlugin_isux(){
	Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu','isux_AddMenu');
	Add_Filter_Plugin('Filter_Plugin_Search_Begin','isux_SearchMain');
	Add_Filter_Plugin('Filter_Plugin_Category_Edit_Response','isux_Category_Edit_Response');
	Add_Filter_Plugin('Filter_Plugin_Tag_Edit_Response','isux_Tag_Edit_Response');
	Add_Filter_Plugin('Filter_Plugin_Edit_Response5','isux_Edit_Response5');
}
function isux_AddMenu(&$m){
	global $zbp;
	array_unshift($m, MakeTopMenu("root",'主题配置',$zbp->host . "zb_users/theme/isux/main.php?act=config","","topmenu_isux"));
}
function isux_tags_set(&$template){
	global $zbp;
    $template->SetTags('isux_keywords',$zbp->Config('isux')->keywords);
    $template->SetTags('isux_description',$zbp->Config('isux')->description);
	$template->SetTags('isux_seo',$zbp->Config('isux')->seo);
	$template->SetTags('isux_category',$zbp->Config('isux')->post_category);
	$template->SetTags('isux_subname',$zbp->Config('isux')->page_subname);
}
function InstallPlugin_isux(){
	global $zbp;
	$zbp->AddBuildModule('searchpanel');
	if(!$zbp->Config('isux')->HasKey('Version')){
		$zbp->Config('isux')->Version = '1.0';
		$zbp->Config('isux')->keywords = '请填写您的网站描述。';
		$zbp->Config('isux')->description = '请填写您的网站关键词，可用英文逗号(,)分开。';
		$zbp->Config('isux')->seo = 'b';
		$zbp->Config('isux')->post_category = 'a';
		$zbp->Config('isux')->page_subname = 'a';
		$zbp->SaveConfig('isux');
	}
}
function isux_TimeAgo( $ptime ) {
    $ptime = strtotime($ptime);
    $etime = time() - $ptime;
    if($etime < 1) return '刚刚';
    $interval = array (
        12 * 30 * 24 * 60 * 60  =>  '年前 ('.date('Y-m-d', $ptime).')',
        30 * 24 * 60 * 60       =>  '个月前 ('.date('m-d', $ptime).')',
        7 * 24 * 60 * 60        =>  '周前 ('.date('m-d', $ptime).')',
        24 * 60 * 60            =>  '天前',
        60 * 60                 =>  '小时前',
        60                      =>  '分钟前',
        1                       =>  '秒前'
    );
    foreach ($interval as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . $str;
        }
    };
}
function isux_Edit_Response5(){
    global $zbp,$article;
	if ($zbp->Config('isux')->seo=="b"){
		echo '<div><label for="meta_gjc"><span style="font-weight: bold;">关键词:</span></label><br></label><input style="width:99%;" type="text" name="meta_gjc" value="'.htmlspecialchars($article->Metas->gjc).'"/></div>';
		echo '<div><label for="meta_ms"><span style="font-weight: bold;">描述:</span></label><br><input style="width:99%;" type="text" name="meta_ms" value="'.htmlspecialchars($article->Metas->ms).'"/></div>';
		echo '<div><label for="meta_fjbt"><span style="font-weight: bold;">附加标题（留空即为不显示）:</span></label><br><input style="width:99%;" type="text" name="meta_fjbt" value="'.htmlspecialchars($article->Metas->fjbt).'"/></div>';
	}
	?>
<script>
function upwindow(){
    var container = document.createElement('script');
	$(container).attr('type','text/plain').attr('id','img_editor');
	$("body").append(container);
	_editor = UE.getEditor('img_editor');
	_editor.ready(function () {
		_editor.hide();
		$(".uploadimg strong").click(function(){        
			object = $(this).parent().find('.uplod_img');
			_editor.getDialog("insertimage").open();
			_editor.addListener('beforeInsertImage', function (t, arg) {
				object.attr("value", arg[0].src);
			});
		});
	});
}
upwindow();
</script>
<?php
	echo '<div class="uploadimg"><label for="meta_thumbnail"><span style="font-weight: bold;">缩略图（1200*600）:</span></label><br><input name="meta_thumbnail" value="'.htmlspecialchars($article->Metas->thumbnail).'" type="text" class="uplod_img" style="width: 60%;" /><strong class="button" style="color: #ffffff;font-size: 1.1em;height: 29px;padding: 6px 18px 6px 18px;margin: 0 0.5em;background: #3a6ea5;border: 1px solid #3399cc;cursor: pointer;">浏览文件</strong></div>';
}
function isux_Tag_Edit_Response(){
    global $zbp,$tag;
	if ($zbp->Config('isux')->seo=="b"){
		echo '<div><label for="meta_gjc"><span style="font-weight: bold;">关键词:</span></label><br></label><input style="width:293px;" type="text" name="meta_gjc" value="'.htmlspecialchars($tag->Metas->gjc).'"/></div>';
		echo '<div><label for="meta_ms"><span style="font-weight: bold;">描述:</span></label><br><input style="width:293px;" type="text" name="meta_ms" value="'.htmlspecialchars($tag->Metas->ms).'"/></div>';
		echo '<div><label for="meta_fjbt"><span style="font-weight: bold;">附加标题（留空即为不显示）:</span></label><br><input style="width:293px;" type="text" name="meta_fjbt" value="'.htmlspecialchars($tag->Metas->fjbt).'"/></div>';
	}
}
function isux_Category_Edit_Response(){
    global $zbp,$cate;
	if ($zbp->Config('isux')->seo=="b"){
		echo '<div><label for="meta_gjc"><span style="font-weight: bold;">关键词:</span></label><br></label><input style="width:293px;" type="text" name="meta_gjc" value="'.htmlspecialchars($cate->Metas->gjc).'"/></div>';
		echo '<div><label for="meta_ms"><span style="font-weight: bold;">描述:</span></label><br><input style="width:293px;" type="text" name="meta_ms" value="'.htmlspecialchars($cate->Metas->ms).'"/></div>';
		echo '<div><label for="meta_fjbt"><span style="font-weight: bold;">附加标题（留空即为不显示）:</span></label><br><input style="width:293px;" type="text" name="meta_fjbt" value="'.htmlspecialchars($cate->Metas->fjbt).'"/></div>';
	}
}
//卸载主题
function UninstallPlugin_isux(){
	global $zbp;
}
?>