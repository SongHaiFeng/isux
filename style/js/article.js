/* 评论框相关 */
zbp.plugin.unbind("comment.reply", "system");
zbp.plugin.on("comment.reply", "Lucky",
function(i) {
    $("#inpRevID").val(i);
    var frm = $('.comment-form'),
    cancel = $("#cancel-comment-reply-link"),
    temp = $('#temp-frm');
    var div = document.createElement('div');
    div.id = 'temp-frm';
    div.style.display = 'none';
    frm.before(div);
    $('#div-comment-' + i).after(frm);
    frm.addClass("reply-frm");
    cancel.show();
    cancel.click(function() {
        $("#inpRevID").val(0);
        var temp = $('#temp-frm'),
        frm = $('.comment-form');
        if (!temp.length || !frm.length) return;
        temp.before(frm);
        temp.remove();
        $(this).hide();
        frm.removeClass("reply-frm");
        return false
    });
    try {
        $('#txaArticle').focus()
    } catch(e) {}
    return false
});
zbp.plugin.on("comment.postsuccess", "system",
function(formData, retString, textStatus, jqXhr) {
	var objSubmit = $("#inpId").parent("form").find(":submit");
	objSubmit.removeClass("loading").removeAttr("disabled").val(objSubmit.data("orig"));
	var data = $.parseJSON(retString);
	if (data.err.code !== 0) {
		alert(data.err.msg);
		throw "ERROR - " + data.err.msg
	}
	if (formData.replyid == "0") {
		$("#comment_list .commentlist").prepend(data.data.html);
	} else {
		$("#comment-"+formData.replyid+"").next(".children").append(data.data.html);
	}
	location.hash = "#comment-" + data.data.ID;
	zbp.$("#txaArticle").val("");
	zbp.userinfo.saveFromHtml()
});
zbp.plugin.on("comment.postsuccess", "isux",
function() {
    $("#cancel-comment-reply-link").click();
});
function c(p, o) {
    p = isNaN(p) ? $("#" + p).offset().top: p;
    $("body,html").animate({
        scrollTop: p
    },
    o ? 0 : 233);
    return false
}
zbp.plugin.on("comment.get", "system", function(postid, page) {
    $("#cancel-comment-reply-link").trigger("click");
    $.get(bloghost + "zb_system/cmd.php?act=getcmt&postid=" + postid + "&page=" + page,
    function(data) {
        $('#AjaxCommentBegin').nextUntil('#AjaxCommentEnd').remove();
        $("#comments_paginate, #comment_list").fadeOut("slow");
        var q = $("#comment_list").html();
        var p = $("#comments_paginate").html();
        $("#comment_list").html(q);
        $("#comments_paginate").html(p);
        c("post-comment-list");
        $("#comments_paginate, #comment_list").fadeIn("slow");
        $('#AjaxCommentBegin').after(data)
    })
});
(function(window,$){

    // 初始化某些标签 
    $('.page-prev a').html('上一篇');
    $('.page-next a').html('下一篇');
    $('.loading-cover').hide();

    var wrap = $(window),
        artical_content = $('.artical-content'),
        wrapWidth = wrap.width(),
        wrapHeight = wrap.height(),
        targetTop = qrcodeTop - wrapHeight + outerHeight,
        targetDom = $('.author-page'),
        qrcode = $('.isux-weixin-qrcode-wrap'),
        qrcodeTop = qrcode.offset().top +qrcode.height(),
        outerHeight = -200,
        animating = false;


    //每个图片加载完成后重新计算高度，确保高度正确
    artical_content.find("p").each(function(){

        $(this).imagesLoaded(function() {
            qrcodeTop = qrcode.offset().top +qrcode.height();
            targetTop = qrcodeTop - wrapHeight + outerHeight;
        });
    });

    wrap.on('resize',function(){
        wrapWidth = wrap.width();
        wrapHeight = wrap.height();

        targetTop = qrcodeTop - wrapHeight + outerHeight;
    });
    wrap.on('scroll',function(e){
        var nowTop = wrap.scrollTop();
        if(!animating && nowTop > targetTop) {

            if(targetDom.hasClass('author-fixed')){
                targetDom.addClass('fixed-to-absolute');

                animating = true;

                setTimeout(function(){
                    targetDom.removeClass('fixed-to-absolute');
                    targetDom.removeClass('author-fixed');
                    animating = false;
                }, 200);
            }
            
        }else if(!animating) {

            if(!targetDom.hasClass('author-fixed')){
                targetDom.addClass('author-fixed');

                targetDom.addClass('absolute-to-fixed');

                animating = true;

                setTimeout(function(){
                    targetDom.removeClass('absolute-to-fixed');
                    animating = false;
                }, 200);
            }
        }
    });


    }(window,jQuery));
