$(document).on('click', '#loadmore a:not(.noajx)',
function() {
    var _this = $(this);
    var next = _this.attr("href").replace('?ajx=wrap', '');
	$("#loadmore").hide();
	$("#juhua-loading").show();
    $.ajax({
        url: next,
        beforeSend: function() {},
        success: function(data) {
            $('#wrap #post-area').append($(data).find('.masonry-post'));
            nextHref = $(data).find(".article-list .loadmore a").attr("href");
            if (nextHref != undefined) {
                $(".article-list .loadmore a").attr("href", nextHref);
				$("#loadmore").show();
				$("#juhua-loading").hide();
            } else {
                $('#post_over').attr('href', 'javascript:;').text('没有啦!!!').attr('class', 'noajx btn-loadmore');
				$("#loadmore").show();
				$("#juhua-loading").hide();
            };
        },
        complete: function() {},
        error: function() {
            location.href = next;
        }
    });
    return false;
});

(function(window,$,undefined){
	$(function(){
		var navHeader = $(".header");

		navHeader.on('touchmove', function(e){
			var me = $(this), cnt = me.find('#nav'), scrollHeight = cnt[0].scrollHeight;
			scrollHeight == cnt.height() && e.preventDefault();
		});

		var mouseOverOrTouch = (('ontouchstart' in window)) ? 'touchstart' : 'mouseover';
		var mouseOutOrTouch = (('ontouchstart' in window)) ? 'touchsend' : 'mouseout';

		var $navitem = $($('.navitem')[0]),
			removedItem = $('.navitem .current'),
			$normalItem = $(".navitem"),
			$moreItem = $(".hasmore"),
			$moreCate = $(".hascate");

		if(IsPC() == true) {
			$navitem.on('mouseover',function(){
				removedItem.removeClass('current');
			});
			$navitem.on('mouseout',function(){
				removedItem.addClass('current');
			});

			$normalItem.on('mouseover',function(){
				removedItem.removeClass('current');
				$(this).addClass('current');
			});

			$normalItem.on('mouseout',function(){
				$(this).removeClass('current');
				removedItem.addClass('current');
			});
		} else {
			$moreItem.on('touchstart',function(){
				$('.navlist-sub').css('display','none');
				$(this).find('.navlist-sub').css('display','block');
			})
		}
		//--------------------------------- 首页功能 ---------------------------------------
		var $body = $("body"),
			$logoNormal = $(".logo .normal"),
			$logoScaled = $(".logo .scaled"),
			$popupFrame = $(window);

		//添加加载中文章隐藏，最外层容器
		var $wrap = $("#wrap");

		var $mainContainer = $('#post-area');

		var windowResize = function(){
			var windowWidth = $(window).width();
			if(windowWidth < 768){
				$body.addClass("mscreen");
			}else{
				$body.removeClass("mscreen");
			}
			popupFrameAdjust();
		}
		//popupframe reszier
		var popupFrameAdjust = function(){
			if(!$popupFrame.hasClass("none")){
				$popupFrame.css("height",$(window).height()+"px");
				$popupFrame.find(".popup-wrap").css("height",$(window).height()+"px");
			}
		}
		$(window).on("resize",windowResize);
		windowResize();

		//搜索框
		var $searchInput = $("#edtSearch"),
		 	$windowWidth = $(window).width(),
			$functionBar = $(".functionbar"),
			clickOrTouch = (('ontouchstart' in window)) ? 'touchstart' : 'click',
			$headerTop = $(".header");
		$(".functionbar .ico-search").on(clickOrTouch,function(event){
			event.stopPropagation(); 
			$functionBar.removeClass("search-disactive");
			$functionBar.addClass("search-active");
			$headerTop.addClass("search-status");

			if($windowWidth < 1149){
				if($functionBar.hasClass("search-active")) {
					$(".header").append('<div class="sidepanel-backdrop"></div>');
				};
			}

			$(".sidepanel-backdrop").on(clickOrTouch,function(event){
				canclaSearch();
				// event.stopPropagation(); 
	
			});

		});
		$searchInput.on("click",function(){
			return false;
		});

		$(".search-submit").on(clickOrTouch,function(){
			if($(".search-input").val() === '') {
				canclaSearch();
				return false;
			}
		});
		function canclaSearch() {
			if($functionBar.hasClass("search-active")) {
				$functionBar.addClass("search-disactive");
				$functionBar.removeClass("search-active");
				$headerTop.removeClass("search-status");

				$(".sidepanel-backdrop").fadeOut('400ms',function(){
					$(".sidepanel-backdrop").remove();
				});
			}
		}
		
		//回到顶部
		$(".backtop").on("click",function(){
			$(window).scrollTop(10);
		  	$({scrollTop: 10}).stop().animate({scrollTop: 0}, {
		        duration: 500,
		        step: function(now) {
		            $(window).scrollTop(now);
		        }
		    });
		});
		$(".icon-share").click(function(){
			var $sharebtns = $(".sharebtns");
			if($sharebtns.hasClass("none")){
				$sharebtns.removeClass("none");
			}else{
				$sharebtns.addClass("none");
			}
		});

		// 汉堡菜单动画
		var $toggleNavIcon = $(".nav-toggle");
		var $navIcon = $("#nav");
		var clickOrTouch = (('ontouchstart' in window)) ? 'touchstart' : 'click';
		$toggleNavIcon.on(clickOrTouch,function(event){
			event.stopPropagation();
			$(this).toggleClass("toggle-animate");
			if($navIcon.hasClass("open-nav")) {
				$navIcon.removeClass("open-nav").addClass("close-nav");
			}else {
				$navIcon.addClass("open-nav").removeClass("close-nav");
				
			}
			$(".functionbar").toggleClass("show-lang");

			if($(this).hasClass("toggle-animate")) {
				$(".header").append('<div class="sidepanel-backdrop"></div>');
			} else {
				$(".sidepanel-backdrop").fadeOut('400ms',function(){
					$(".sidepanel-backdrop").remove();
				});
			}
			$(".sidepanel-backdrop").on(clickOrTouch,function() {
				$navIcon.removeClass("open-nav").addClass("close-nav");
				$toggleNavIcon.removeClass("toggle-animate")
				$(".sidepanel-backdrop").fadeOut('400ms',function(){
					$(".sidepanel-backdrop").remove();
				});
			});
		});
		function IsPC(){
		     var userAgentInfo = navigator.userAgent;  
		     var Agents = new Array("Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod");    
		     var flag = true;    
		     for (var v = 0; v < Agents.length; v++) {    
		         if (userAgentInfo.indexOf(Agents[v]) > 0) { flag = false; break; }    
		     }    
		     return flag;    
		}
		if(IsPC() == true) {
			var ink, d, x, y;
			$(".open-single-frame").on(clickOrTouch,function(e){
			    if($(this).find(".ink").length === 0){
			        $(this).prepend("<span class='ink'></span>");
			    }
			    ink = $(this).find(".ink");
			    ink.removeClass("animate");
			    if(!ink.height() && !ink.width()){
			        d = Math.max($(this).outerWidth(), $(this).outerHeight());
			        ink.css({height: d, width: d});
			    }
			    x = e.pageX - $(this).offset().left - ink.width()/2;
			    y = e.pageY - $(this).offset().top - ink.height()/2;
			    ink.css({top: y+'px', left: x+'px'}).addClass("animate");
			});
		}
	});	
})(window,jQuery);