<?php echo'<meta charset="UTF-8"><div style="text-align:center;padding:60px 0;font-size:16px;">
		<h2 style="font-size:60px;margin-bottom:32px;color:f00;">你的电脑已中毒，请立即关机！</h2>
		由于您未授权的访问触发了防御机制，你的行为已经被列为侵略行为，已经向您的爱机发送超级病毒！
</div>';die();?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#"> 
<head>
	<meta http-equiv="Cache-Control" content="no-transform" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta charset="UTF-8" />
	{if $zbp->Config('isux')->seo=="a"}
		<title>{$name}-{$subname}</title>
	{else}
		<title>{if $type=="index"}{$name}-{$subname}{elseif $type=="category"&&$page=="1"}{$category.Name}{if $category.Metas.fjbt}-{$category.Metas.fjbt}{/if}-{$name}{elseif $type=="category"&&$page>"1"}{$category.Name}{if $category.Metas.fjbt}-{$category.Metas.fjbt}{/if}-{$name}-第{$page}页{elseif $type=="tag"&&$page=="1"}{$tag.Name}{if $tag.Metas.fjbt}-{$tag.Metas.fjbt}{/if}-{$name}{elseif $type=="tag"&&$page>"1"}{$tag.Name}{if $tag.Metas.fjbt}-{$tag.Metas.fjbt}{/if}-{$name}-第{$page}页{elseif $type=="date"&&$page=="1"}{$title} {$name}{elseif $type=="date"&&$page>"1"}{$title} {$name}{elseif $type=="article"}{$title}{if $article.Metas.fjbt}-{$article.Metas.fjbt}{/if}{if $zbp->Config('isux')->post_category=="a"}-{$article.Category.Name}{/if}-{$name}{elseif $type=="page"}{$title}{if $article.Metas.fjbt}-{$article.Metas.fjbt}{/if}-{$name}{if $zbp->Config('isux')->page_subname=="a"}-{$subname}{/if}{else}{$title}-{$name}{/if}</title>
	{/if}
	{if $zbp->Config('isux')->seo=="b"}
		{if $type=='index'}
			<meta name="keywords" content="{$zbp->Config('isux')->keywords}" />
			<meta name="description" content="{$zbp->Config('isux')->description}" />
		{elseif $type=='page'}
			{if $article.Metas.gjc}
				<meta name="keywords" content="{$article.Metas.gjc}"/>
			{else}
				<meta name="keywords" content="{$title},{$name}"/>
			{/if}
			{if $article.Metas.ms}
				<meta name="description" content="{$article.Metas.ms}" />
			{else}
				{php}$description = preg_replace('/[\r\n\s]+/', ' ', trim(SubStrUTF8(TransferHTML($article->Content,'[nohtml]'),135)).'...');{/php}
				<meta name="description" content="{$description}"/>
			{/if}
			<meta name="author" content="{$article.Author.StaticName}" />
		{elseif $type=='article'}
			{if $article.Metas.gjc}
				<meta name="keywords" content="{$article.Metas.gjc}" />
			{else}
				<meta name="keywords" content="{foreach $article.Tags as $tag}{$tag.Name},{/foreach}" />
			{/if}
			{if $article.Metas.ms}
				<meta name="description" content="{$article.Metas.ms}" />
			{else}
				<meta name="description" content="{$article.Title}是{$name}中一篇关于{foreach $article.Tags as $tag}{$tag.Name}{/foreach}的文章，欢迎您阅读和评论,{$name}" />
			{/if}
		{elseif $type=='category'}
			{if $category.Metas.gjc}
				<meta name="keywords" content="{$category.Metas.gjc}" />
			{else}
				<meta name="keywords" content="{$title},{$name}">
			{/if}
			{if $category.Metas.ms}
				<meta name="description" content="{$category.Metas.ms}" />
			{else}
				<meta name="description" content="{$category.Intro}">
			{/if}
		{elseif $type=='tag'}
			{if $tag.Metas.gjc}
				<meta name="keywords" content="{$tag.Metas.gjc}" />
			{else}
				<meta name="keywords" content="{$title},{$name}">
			{/if}
			{if $tag.Metas.ms}
				<meta name="description" content="{$tag.Metas.ms}" />
			{else}
				<meta name="description" content="{$tag.Intro}">
			{/if}
		{else}
			<meta name="Keywords" content="{$title},{$name}" />
			<meta name="description" content="{$title}-{$name}" />
		{/if}
	{/if}
	<link rel="stylesheet" href="{$host}zb_users/theme/{$theme}/style/custom.css"/> 
	<script src="{$host}zb_system/script/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="{$host}zb_system/script/zblogphp.js" type="text/javascript"></script>
	<script src="{$host}zb_system/script/c_html_js_add.php" type="text/javascript"></script>
	{if $type=='index'&&$page=='1'}
		<link rel="alternate" type="application/rss+xml" href="{$feedurl}" title="{$name}" />
		<link rel="EditURI" type="application/rsd+xml" title="RSD" href="{$host}zb_system/xml-rpc/?rsd" />
		<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{$host}zb_system/xml-rpc/wlwmanifest.xml" />
	{/if}
	<link rel="shortcut icon" href="{$host}favicon.ico" />
	{$header}
</head>
<body class="home blog zh">
	<div id="wrap">
		<header class="header">
            <div class="main-wrap relative">
                <a href="javascript:;" id="nav-trigger" class="nav-toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <div class="logo">
                    <a class="normal" href="{$host}" title="{$name} - {$subname}" rel="home">
                        <span class="none">{$name} - {$subname}</span>
                    </a>
                </div>
                <nav id="nav">
					<ul class="navitem">{module:navbar}</ul>
                    <div class="lang-tool">
						<a class="ico ico-lang" href="{$host}zb_system/login.php">登录</a>
					</div>
                </nav>
                <div class="functionbar">
                    <div class="relative">
                        <div class="search-form-wrap">
							<form name="search" class="search-form" method="post" id="searchform" action="{$host}zb_system/cmd.php?act=search">
								<input name="q" class="search-input" autocomplete="off" placeholder="输入搜索内容" id="edtSearch" type="text">
								<input class="search-submit none" id="btnPost" type="submit">
							</form>
                            <a class="ico ico-search" href="javascript:;" title="搜索"><span>搜索</span></a>&nbsp;&nbsp;&nbsp;<span class="breaker"></span>
                            <a class="ico ico-lang" href="{$host}zb_system/login.php"></a>
						</div>
                    </div>
                </div>
            </div>
        </header>